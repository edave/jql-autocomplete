#!/bin/bash

./node_modules/pegjs/bin/pegjs lib/grammer/JQL.pegjs && cat > ../jira-issue-nav-plugin/src/main/resources/content/js/issue-nav/jql/JQL.Parser.js <<HERE
AJS.namespace("JIRA.Issues.JQLParser");

JIRA.Issues.JQLParser = function() {
`perl -e '@l=<>;print splice@l,1,-1;' lib/grammer/JQL.js`
};
HERE

