var fs = require('fs'),
	PEG = require("pegjs"),
	grammer = fs.readFileSync(__dirname + "/grammer/JQL.pegjs", "utf8"),
	parser = PEG.buildParser(grammer),
	util = require('util');

module.exports = {
	parse: function(statement) {
	    try {
    		console.log("PARSING: '" + statement + "'");
    		var result = parser.parse(statement);
    		console.log("PARSE RESULT:\n", util.inspect(result, false, 15), "\n\n");
	        return result
	    } catch (e) {

    		console.log("problem: '" + statement.substr(e.column - 1) + "'");
    		console.log(e);
    		console.log("-- -- --")
//process.exit();
	    }
	}
};
