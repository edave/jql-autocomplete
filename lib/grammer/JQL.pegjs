{
    var operatorLookup = {
        "&&": "and",
        "&": "and",
        "||": "or",
        "|": "or"
    }

    var sequence = 0,
        broadcast = function() {};

    if (!broadcast) {
        broadcast = function() {};
    }

    function broadcast(text, pos, type, obj) {
        //    console.log("Found", type, "'" + text + "'" + " at " + pos);
        if (typeof jqlInputElement !== "undefined" && jqlInputElement.trigger) {
            var cursorPosition = parseInt(jqlInputElement.attr("cursorPosition")),
                start = pos - text.length,
                payload = {
                    value: text,
                    start: start,
                    end: pos,
                    cursorPosition: cursorPosition,
                    type: type
                };

            if (obj) {
                jQuery.extend(payload, obj);
            }

            jqlInputElement.trigger("JQL.parserFound" + type, [payload]);
        }
    }
}


query
    = _ order:OrderBy _ { return { order: order } }
    / clause:Clause __ order:OrderBy _ { return { clause: clause, order: order } }
    / clause:Clause _ { return { clause: clause } }
    / _


Clause
    = orClause

orClause
    = ac : andClause OR oc:orClause {
        return {
            type: "or",
            left: ac,
            right: oc
        }
    }
    / andClause

andClause
    = nc : notClause AND ac:andClause {
        return {
            type: "and",
            left: nc,
            right: ac
        }
    }
    / notClause

notClause
    = n:NEGATOR* c:(subClause / terminalClause) {
        if (n && n.length && n.length % 2 !== 0) {
            c.not = true;
        }

        return c;
    }

subClause
    = OPEN_PAREN _ oc:orClause _ CLOSE_PAREN {
        return {
            type: "sub",
            clause: oc
        }
    }

terminalClause
    = field:Field operator:Operator operand:Operand hp: historyPredicate? {
        var ret = {
            field: field,
            operator: operator,
            operand: operand
        }
        if (hp) {
            ret.historyPredicate = hp
        }

        return ret;
    }
    / field:Field __ CHANGED hp:historyPredicate? {
        var ret = {
            field: field,
            operator: "changed"
        }
        if (hp && hp.length) {
            ret.historyPredicate = hp
        }

        return ret;
    }

historyPredicate
    = terminal:terminalHistoryPredicate rest:historyPredicate? {
        if (rest && rest.length > 0) {
            rest.unshift(terminal);
            return rest;
        } else {
            return [terminal]
        }
    }

terminalHistoryPredicate
    = __ operator:historyPredicateOperator operand:Operand {
        return {
            operator: operator,
            operand: operand
        };
    }


historyPredicateOperator
    = FROM
	/ TO
	/ BY
	/ BEFORE
	/ AFTER
	/ ON
	/ DURING


OPEN_PAREN
    = p:"(" {
        broadcast(p, pos, "OpenParen");
        return p;
    }

CLOSE_PAREN
    = p:")" {
        broadcast(p, pos, "CloseParen");
        return p;
    }


CHANGED "changed"
    = c:"changed"i {
        broadcast(c, pos, "Operator");
        return c;
    }

ChangedOperator
    = __ operator:(FROM / TO / AFTER / BEFORE / DURING / ON / BY) __ {
        operator = operator;
        broadcast(operator, pos, "ChangedClauseOperator");
        return operator;
    }

Operator "Operator"
    = _ o:CharacterBasedOperator _ {
        var operator = o;
        broadcast(operator, pos, "Operator")
        return operator;
    }
    / __ operator:( WAS_NOT_IN / NOT_IN / WAS_IN / IN ) !BareWordCharacter {
        broadcast(operator, pos, "Operator")
        return operator; 
    }
    / __ o:( WAS_NOT / IS_NOT / IS / WAS ) __ {
        var operator = o;
        broadcast(operator, pos, "Operator")
        return operator;
    }

CharacterBasedOperator
    = DoubleCharacterOperator
    / SingleCharacterOperator

DoubleCharacterOperator
    = ">=" / "<=" / "!=" / "!~"

SingleCharacterOperator
    = "=" / ">" / "<" / "~"

Operand "Operand"
    = _ o:(NULL / EMPTY / Function / String / Number / List) {
        broadcast(o, pos, "Operand");
        return o;
    }

OrderBy
    = ORDER_BY field:OrderByField list:OrderByFieldList+ _ {
        list.unshift(field);
        return list;
    }

    / ORDER_BY field:OrderByField _ {
        return [field];
    }

OrderByFieldList
    = COMMA o:OrderByField {
        return o;
    }

OrderByField "orderbyfield"
    = field:Field _ direction:OrderDirection {
        return {
            field: field,
            direction: (direction.length) ? direction : "asc"
        };
    }

    / field:Field {
        return {
            field: field,
            direction: "asc"
        };
    }

Field "field"
    = EMPTY
    / CustomField
    / field:NonEmptyString {
        var re = /^\s+$/i;
        if (re.test(field)) {
            return null;
        }

        broadcast(field, pos, "Field");
        return field;
    }

CustomField
    = CF s1:_ "[" s2:_ field:Integer s3:_ "]" {
        var finalField = "cf" + s1 + "[" + s2 + field + s3 + "]";
        broadcast(finalField, pos, "Field");

        return finalField;
    }

String
    =  StringLiteral / BareWord

StringLiteral "string"
    = parts:('"' DoubleStringCharacters '"' / "'" SingleStringCharacters "'") {
        return parts[1];
    }

NonEmptyStringLiteral
    = parts:('"' OneOrMoreDoubleStringCharacters '"' / "'" OneOrMoreSingleStringCharacters "'") {
        return parts[1];
    }

NonEmptyString
    = NonEmptyStringLiteral / BareWord
//a bit hacky. there must be a nicer way to do quoted strings
OneOrMoreDoubleStringCharacters
    = chars:DoubleStringCharacter+ { return chars.join(""); }

OneOrMoreSingleStringCharacters
    = chars:SingleStringCharacter+ { return chars.join(""); }

DoubleStringCharacters
    = chars:DoubleStringCharacter* { return chars.join(""); }

SingleStringCharacters
    = chars:SingleStringCharacter* { return chars.join(""); }


DoubleStringCharacter
    = !DOUBLE_QUOTE char_:LiteralCharacter { return char_; }
    / EscapeSequence

SingleStringCharacter
    = !SINGLE_QUOTE char_:LiteralCharacter { return char_; }
    / EscapeSequence

EscapeSequence
    = bs:BSLASH escaped:EscapableCharacter {
        return escaped;
    }
    / UnicodeEscapeSequence

EscapableCharacter
    = char_:['"\\bfnrtv ] {
      return char_
        .replace("b", "\b")
        .replace("f", "\f")
        .replace("n", "\n")
        .replace("r", "\r")
        .replace("t", "\t")
    }

BSLASH
    = "\\"

Connective "Connective"
    = ConnectiveWord
    / _ j:("&&" / "&" / "||" / "|" ) _ { return operatorLookup[j]; }

ConnectiveWord
    = __ j:( AND / OR ) __ { return j.toLowerCase(); }

ReservedWord
    = ConnectiveWord / NOT

BareWord
    = word:BareWordCharacter+ {
        var bareword = word.join("").toLowerCase();
        if (JIRA.JQL.Util.getReservedWords().indexOf(bareword) > -1) {
            return null;
        }
        return word.join("");
    }

BareWordCharacter
    = !(BSLASH / " " / IllegalBareWordCharacter) char_:. { return char_; }
    / EscapeSequence
  
LiteralCharacter
    = !(IllegalUnicodeCharacter / BSLASH) char_:. { return char_; }

// Stolen from JQL.g
// The following are Unicode non-characters. We don't want to parse them. Importantly, we wish 
// to ignore U+FFFF since ANTLR evilly uses this internally to represent EOF which can cause very
// strange behaviour. For example, the Lexer will incorrectly tokenise the POSNUMBER 1234 as a STRING
// when U+FFFF is not excluded from STRING.
//
// http://en.wikipedia.org/wiki/Unicode
// 
IllegalUnicodeCharacter
    = [\u0000-\u0009]
    / [\u000b-\u000c]
    / [\u000e-\u001f]
    / [\u007f-\u009f]
    / [\ufdd0-\ufdef]
    / [\ufffe-\uffff]

HexDigit
    = [0-9a-fA-F]

UnicodeEscapeSequence
    = "\\u" h1:HexDigit h2:HexDigit h3:HexDigit h4:HexDigit {
        return String.fromCharCode(parseInt("0x" + h1 + h2 + h3 + h4));
    }

IllegalBareWordOperatorCharacter
    = SingleCharacterOperator
    / BANG
    / [{}*/%+^$#@?.;'()[\]|&,]
    / NL

IllegalBareWordCharacter
    = IllegalUnicodeCharacter
    / ['"\\ ]
    / IllegalBareWordOperatorCharacter

Number "number"
    = i:(SignedInteger / Float) {
        return i.toString();
    }
    
SignedInteger
    = sign:[-]? digits:Integer {
        return [sign, digits.join("")].join("");
    }

Integer = digits:[0-9]+ {
        return digits.join("");
    }

Float
    = before:[0-9]* "." after:[0-9]+ {
        return [before.join(""), after.join("")].join(".");
    }

List "list"
    = _ OPEN_PAREN _ param:Operand _ params:Param+ _ CLOSE_PAREN {
        params.unshift(param);
        return params;
    }

    / _ OPEN_PAREN param:Operand CLOSE_PAREN {
        return [param];
    }

Param
    = COMMA o:Operand { return o; }

Function
    = functionName:NonEmptyString _ params:List {
        return {
            functionName: functionName,
            params: params
        }
    }

    / functionName:NonEmptyString _ "(" _ ")" {
        return {
            functionName: functionName,
             params: []
        }
    }

/**
 * JQL Operators
 */

WAS "was"
    = w:"was"i {
        return w;
    }

NEGATOR "negator"
    = n:NOT __ {
        broadcast(n, pos, "Negator");
        return n;
    }
    / n:BANG _ {
        broadcast(n, pos, "Negator");
        return n;
    }
 
ORDER_BY "orderby"
    = "order"i __ "by"i __ {
        broadcast("order by", pos, "OrderBy");
    }

OrderDirection "orderdirection"
    = d:(DESC / ASC) {
        broadcast(d, pos, "OrderDirection");
        return d;
    }

COMMA "comma"
    = _ c:"," _ {
        broadcast(c, pos, "Comma");
        return c;
    }

AND "and"
    = __ "and"i __ / _ (AMPER_AMPER / AMPER) _
    
OR "or"
    = __ "or"i __ / _ (PIPE_PIPE / PIPE) _
    
OR_NOT "or_not"
    = OR __ NOT
    
AND_NOT "and_not"
    = AND __ NOT

NOT "not"
    = n:"not"i {
        return n;
    }
    
BANG 
    = '!'

NOT_IN
    = not:NOT __ inn:IN {
        return not + " " + inn;
    }
    / BANG IN

WAS_NOT_IN
    = w:WAS __ n:NOT __ i:IN {
        return [w, n, i].join(" ");
    }

WAS_IN
    = w:WAS __ i:IN {
        return [w, i].join(" ");
    }

WAS_NOT
    = w:WAS __ n:NOT {
        return [w, n].join(" ");
    }

IN 
    = o:"in"i {
        return o;
    }

IS 
    = is:"is"i {
        return is;
    }
    
IS_NOT
    = is:IS __ not:NOT {
        return is + " " + not;
    }
    
EMPTY 
    = "empty"i { return "empty"; }

NULL
    = "null"i { return "null"; }

BEFORE 
    = "before"i { return "before"; }
    
AFTER
    = "after"i { return "after"; }
    
FROM
    = "from"i { return "from"; }
    
TO
    = "to"i { return "to"; }

ON 
    = "on"i { return "on"; }
    
DURING
    = "during"i { return "during"; }

ORDER 
    = "order"i { return "order"; }
    
BY
     = "by"i { return "by"; }

ASC
    = a:"asc"i { return a.toLowerCase(); }
    
DESC
    = d:"desc"i { return d.toLowerCase(); }

CF
    = "cf"i { return "cf"; }

AMPER
    = '&'

AMPER_AMPER
    = '&&'
    
PIPE
    = '|'
    
PIPE_PIPE
    = '||'

DOUBLE_QUOTE
    = '"'
  
SINGLE_QUOTE
    = "'"

QUOTE
    = DOUBLE_QUOTE
    / SINGLE_QUOTE

OQ "Option quote"
    = QUOTE?

NL
    = "\n"
    / "\r"

_ "optional white space"
    = s:[ \t\n\r]* {
        return s.join("");
    }

__ "required white space"
    = s:[ \t\n\r]+ {
        return s.join("");
    }
