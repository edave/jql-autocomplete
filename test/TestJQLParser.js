var fs = require('fs'),
	PEG = require("pegjs"),
	grammer = fs.readFileSync(__dirname + "/../lib/grammer/JQL.pegjs", "utf8"),
	parser = PEG.buildParser(grammer),
	util = require('util');

    var illegalCharsString = "{}*/%+$#@?.;";

    JIRA = {};
    JIRA.JQL = {};
    JIRA.JQL.Util = {};

    JIRA.JQL.Util.getReservedWords = function() {
        return ["greater","for","privileges","float","validate","distinct","of","break","defaults","byte"
            ,"initial","file","noaudit","empty","on","false","boolean","right","option","decrement","limit","else"
            ,"increment","fetch","equals","or","number","table","like","create","row","declare","not","trans","asc"
            ,"start","session","then","view","strict","explain","go","unique","desc","raise","exclusive","before"
            ,"next","inout","goto","date","nowait","escape","mode","character","rownum","union","encoding","delete"
            ,"current","whenever","left","do","null","end","min","trigger","intersection","define","max","previous"
            ,"integer","sqrt","return","true","checkpoint","divide","join","access","alter","field","delimiter"
            ,"string","exists","modulo","having","public","insert","abort","uid","to","last","grant","count"
            ,"transaction","synonym","inner","char","drop","rename","collate","by","where","long","identified"
            ,"prior","function","revoke","after","remainder","values","more","commit","when","any","power","notin"
            ,"returns","avg","index","execute","minus","select","int","double","size","rows","and","difference","input"
            ,"default","isempty","intersect","column","exec","output","cf","update","raw","connect","set","catch","sum"
            ,"object","from","add","collation","while","share","order","isnull","if","less","between","all","with","is"
            ,"check","alias","resource","lock","into","modify","audit","as","multiply","in","decimal","begin","subtract"
            ,"immediate","outer","continue","group","user","rowid","first"
        ];
    }

//test(function() {
//    var result = parser.parse("string");
//    assertEqual(result, {
//        "operand": "string"
//    })
//})
// <span class="clause and"><span class="field">assignee</span> <span class="operator">=</span> <span class="operand">currentUser()</span></span>

var failures = 0;
function _testSuccess(query) {
    try {
//        test("", function () {
//            deepEqual(JIRA.JQL.Parser.parse("priority=\"qwerty\""),
//                { clause:{ field:'priority', operator:'=', operand:'qwerty' } }
//            );
//        });
        var result = parser.parse(query);
        console.log("test(\"\", function () {\ndeepEqual(JIRA.JQL.Parser.parse(\"" + query + "\"),\n" + util.inspect(result, false, 10) + "\n);\n});");

        console.log(" ")
    } catch (e) {
        var spaces = [];
        for (var i = 0; i < e.column - 1; i++) {
            spaces.push("");
        }
        console.log("Testing for success but failed with: ", query);
        console.log(++failures, "failures");
        console.log("problem: '" + query + "'");
        console.log("          " + spaces.join(" "), "^");
        console.log(e);
        console.log("----------")
        console.log("----------")
        //process.exit();
    }
}

function _testFailure(query) {
  try {
      var result = parser.parse(query);
      console.log("Invalid query passed: '" + query + "'");
      console.log(++failures, "failures");
      console.log(util.inspect(result, false, 10));
      console.log("----------")
      console.log("----------")

//    process.exit();
  } catch (e) {
  }
}

_testSuccess("priority = \"qwerty\"");
_testSuccess("priority=\"qwerty\"");
_testSuccess("priority=qwerty");
_testSuccess("priority=     qwerty order      by priority, other");
_testSuccess("priority=     qwerty "); // Test with trailing space

//Checking minus after removing it from the reserved characters list.
_testSuccess("key = one-1");
_testSuccess("key in (one-1, -1)");
_testSuccess("key in (one-1, 1-1)");
_testSuccess("-78a = a");
_testSuccess("numberfield >= -29202");
_testSuccess("numberfield >= -29202-");
_testSuccess("numberfield >= w-88 ");

//Test to ensure that newline is accepted.
_testSuccess("newline = \"hello\nworld\"");
_testSuccess("newline = \"hello\\nworld\"");
_testSuccess("newline = 'hello\r'");
_testSuccess("newline = 'hello\\r'");
_testSuccess("newline = '\r'");
_testSuccess("newline = '\\r'");
_testSuccess("'new\nline' = 'b'");
_testSuccess("'new\\nline' = 'b'");
_testSuccess("'newline' = 'fun\rc'()");
_testSuccess("'newline' = 'fun\\rc'()");

//Some tests for the other operators.
_testSuccess("coolness >= awesome");
_testSuccess("coolness > awesome");
_testSuccess("coolness < awesome");
_testSuccess("coolness <= awesome");
_testSuccess("coolness        !=       awesome order     by     coolness desc");

//Some tests for the in operator.
_testSuccess("language in (java, c, \"python2\")");
_testSuccess("languagein   IN    (   java, c     , \"python2\")");
_testSuccess("inlanguage in (java, c, \"python2\")");
_testSuccess("pri in (java,c,\"python2\")");
_testSuccess("pri in(java)");
_testSuccess("pri In(java)");
_testSuccess("pri iN(java)");

//Some tests for the NOT in operator.
_testSuccess("language not in (java, c, \"python2\")");
_testSuccess("languagein  NOT   IN    (   java, c     , \"python2\")");
_testSuccess("inlanguage not in (java, c, \"python2\")");
_testSuccess("pri NOT in (java,c,\"python2\")");
_testSuccess("pri not in(java)");
_testSuccess("pri NoT In(java)");
_testSuccess("pri nOT iN(java)");

// Some tests for the LIKE operator.
_testSuccess("pri ~ stuff");
_testSuccess("pri~stuff");
_testSuccess("pri ~ 12");
_testSuccess("pri~12");
_testSuccess("pri ~ (\"stuff\", 12)");

// Some tests for the NOT_LIKE operator.
_testSuccess("pri !~ stuff");
_testSuccess("pri!~stuff");
_testSuccess("pri !~ 12");
_testSuccess("pri!~12");
_testSuccess("pri !~ (\"stuff\", 12)");

// Some tests for the IS operator
_testSuccess("pri IS stuff");
_testSuccess("pri is stuff");
_testSuccess("pri IS EMPTY");

// Some tests for the IS_NOT operator
_testSuccess("pri IS NOT stuff");
_testSuccess("pri IS not stuff");
_testSuccess("pri is Not stuff");
_testSuccess("pri is not stuff");


//Test for the nested behaviour of in clause.
_testSuccess("pri iN((java), duke)");

//Test to make sure that numbers are returned correctly.
_testSuccess("priority = 12345");
_testSuccess("priority = -12345");
_testSuccess("priority = \"12a345\"");
_testSuccess("priority = 12345a");

//Test custom field labels
_testSuccess("cf[12345] = 12345a");
_testSuccess("Cf  [ 0005 ] = x");

//Make sure that a quoted number is actually returned as a string.
_testSuccess("priority = \"12345\"");

//An invalid number should be returned as a string.
_testSuccess("priority=\"12a345\"");

//Some tests to check the empty operand
_testSuccess("testfield = EMPTY");
_testSuccess("testfield = empty");
_testSuccess("testfield = NULL");
_testSuccess("testfield = null");
_testSuccess("testfield = \"null\"");
_testSuccess("testfield = \"NULL\"");
_testSuccess("testfield = \"EMPTY\"");
_testSuccess("testfield = \"empty\"");

// tests for quoted strings with characters that must be quoted
_testSuccess("priority = \"a big string ~ != foo and priority = haha \"");
_testSuccess("priority = \"\"");

//test for strange field names.
_testSuccess("prior\\'ty = testvalue");
_testSuccess("priority\\ ty=testvalue");
_testSuccess("priority\u2ee5 > 6");
_testSuccess("priori\\nty\\u2ee5 > 6");
_testSuccess("\"this is a strange field \" = google");
_testSuccess("\"don't\" = 'true'");
_testSuccess("\"don\\\"t\" = 'false'");
_testSuccess("\"don't\" = 'false'");
_testSuccess("'don\\'t' = 'false' order by 'don\\'t' DEsc");
_testSuccess("'don\"t' = 'false'");
_testSuccess("'cf[1220]' = abc");
_testSuccess("'cf' = abc");
_testSuccess("10245948 = abc      order          by 10245948");
_testSuccess("-10245948 = abc");
_testSuccess("new\\nline = abc");
_testSuccess("some\\u0082control = abc  order by some\\u0082control");

//test for strange field values.
_testSuccess("b = ''");
_testSuccess("b = \\ ");
_testSuccess("labels = \\  and labels = s");
_testSuccess("b = don\\'t\\ stop\\ me\\ now");
_testSuccess("b = \u2ee5");
_testSuccess("b = \\u2EE5jkdfskjfd");
_testSuccess("b not in 'jack says, \"Hello World!\"'");
_testSuccess("b not in 'jack says, \\'Hello World!\\''");
_testSuccess("b not in \"jack says, 'Hello World!'\"");
_testSuccess("b not in \"jack says, \\\"Hello World!'\\\"\"");
_testSuccess("b not in \"jack says, \\tnothing\"");
_testSuccess("bad ~ wt\\u007f");

//tests for escaping.
_testSuccess("priority = \"a \\n new \\r line\"");
_testSuccess("priority = \"Tab:\\t NewLine:\\n Carrage Return:\\r\"");
_testSuccess("priority = \"Quote:\\\" Single:\\' Back Slash:\\\\ Space:\\ \"");
_testSuccess("priority = \"Unicode: \\ufeeF1 Unicode2: \\u6EEF\"");
_testSuccess("priority = 'Escape\" don\\'t'");
_testSuccess("priority = \"Escape' don\\\"t\"");

//Some tests for function calls.
_testSuccess("priority = higherThan(Major)");
_testSuccess("priority In     randomName(Major, Minor,      \"cool\", -67485)");
_testSuccess("priority    >=    randomName()");
_testSuccess("pri not in func(99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999)");
_testSuccess("pri not in fun(name\\u0082e)");

//test for strange function names.
_testSuccess("a = func\\'  ()");
_testSuccess("a = fu\\\"nc\\'()");
_testSuccess("a=function\\ name(  )");
_testSuccess("a = \u2ee5()");
_testSuccess("a = somereallystrangestring\\u2ee5()");
_testSuccess("version <= \"affected\\ versions\"(   )");
_testSuccess("version <= \"affected\\ versio'ns\"(   )");
_testSuccess("version <= \"affected versio\\\"ns\"(   )");
_testSuccess("version <= 'my messed up versio\\'ns'     (   )");
_testSuccess("version <= 'my m\\nessed up\\ versio\"ns'     (   )");
_testSuccess("version <= 4759879855`(   )");
_testSuccess("version <= 4759879(   )");
_testSuccess("version = badname\\u0091");


//test some of the string breaks
_testSuccess("a=b&c=d");
_testSuccess("a=b&&c=d");
_testSuccess("a=b|c=d");
_testSuccess("a=b||c=d");
_testSuccess("a<b");
_testSuccess("a>b");
_testSuccess("a~b");

//Check the and operator.
_testSuccess("priority = major and foo > bar()");
_testSuccess("priority = majorand and foo>bar()");
_testSuccess("priority = major and foo > bar()");
_testSuccess("priority != major    and      foo >      bar()");
_testSuccess("priority != major    &&      foo >      bar()");
_testSuccess("priority != andmajor    &      foo >      bar()");
_testSuccess("priority != andmajor    and      foo >      bar() order by priority     DESC,      foo");

//Check the or operator.
_testSuccess("priority = major or foo > bar()");
_testSuccess("priority = major or foo > bar()");
_testSuccess("priority = major or foo > bar() or priority = minor");
_testSuccess("priority = major || foo > bar() | priority = minor");
_testSuccess("priority = major or foo > bar() || priority = minor");

//Checks for operator precedence for and and or.
//    final Clause fooGtBarFunc123 = new TerminalClauseImpl("foo", GREATER_THAN, new FunctionOperand("bar", Arrays.asList("1", "2", "3")));
//    final Clause priorityFooAnd = new AndClause(priorityEqualsMajor, fooGtBarFunc123);
//    final Clause bazNotEqual1234 = new TerminalClauseImpl("baz", NOT_EQUALS, 1234L);
//    final Clause priorityBazAnd = new AndClause(priorityEqualsMinor, bazNotEqual1234);
//    final Clause outerOr = new OrClause(priorityFooAnd, priorityBazAnd);
_testSuccess("priority = major and foo > bar(1,2,3) | priority = minor and baz != 1234");
_testSuccess("priority =     major AND foo > bar(1,2,3) oR priority = minor and baz != 1234");
_testSuccess("priority=major and foo>bar(1,2,3)|| priority=minor  and  baz!=1234");
_testSuccess("priority = major AND foo > bar(1,2,3) Or priority = minor AND baz != 1234");

//Another test for the and operator.
_testSuccess("priority = major && foo > bar(1,2,3) & priority = minor and baz != 1234");

// use parentheses to overthrow precedence
_testSuccess("priority = major and (foo > bar(1,2,3) | priority = minor) and baz != 1234");

//make sure the precedence still works with the brackets.
_testSuccess("priority = major or (foo > bar(1,2,3) or priority = minor) && baz != 1234");

//test for the not operator.
_testSuccess("not priority = major or foo > bar() or priority = minor");
_testSuccess("not priority = major or foo > bar() AnD priority=\"minor\"");
_testSuccess("not priority = major or not foo > bar() AnD priority=\"minor\"");
_testSuccess("not (priority = major or not foo > bar()) AnD priority=\"minor\"");

//check the '!' operator.
_testSuccess("! (priority = major or ! foo > bar()) AnD priority=\"minor\"");

//test that a double not also works.
_testSuccess("not ! (! priority = major or     foo >bar()) &         priority=\"minor\"");

//Tests to make sure illegal characters can be escaped.
for (var i = 0; i < illegalCharsString.length; i++) {
    var currentChar = illegalCharsString.charAt(i);
    var fieldName = ["'test", currentChar, "dfjd'"].join("");
    _testSuccess([fieldName, "= 'good'"].join(""));
}

//Make sure the reserved words can be escaped.
var reservedWords = JIRA.JQL.Util.getReservedWords();
for (var i = 0; i < reservedWords.length; i++) {
    var reservedWord = reservedWords[i];
    _testSuccess(["priority NOT IN ('", reservedWord, "')"].join(""));
}

//We want to be able to add sort when there is no where clause.
_testSuccess("order by crap");
_testSuccess("order by crap  DESC");
_testSuccess("order by crap  ASC");
_testSuccess("order by cf[12345]  ASC");

_testSuccess("");
_testSuccess("       ");

//Some tests for invalid parsing input.
_testFailure("  \t  \n\r\f   "); //Tabs and Spaces String
_testFailure("foo"); //Bare field
_testFailure("foo="); //Missing operand
_testFailure("="); //Just an operator
_testFailure("!="); //Just a two character operator
_testFailure("foo bar and 78foo = bar"); //Missing operator in first subclause of otherwise OK composite logical clause
_testFailure("and and"); //Two logical operators only
_testFailure("a=b a=b"); //missing logical operator
_testFailure("foo=bar and"); //Legal subclause with missing second subclause
_testFailure("foo=bar and and"); //Legal subclause with malformed second subclause
_testFailure("foo=bar and 78foo"); //Incomplete second subclause
_testFailure("foo=bar and 78foo="); //Incomplete second subclause2
_testFailure("foo=bar and \n78foo"); //Incomplete second subclause with newline
_testFailure("foo=bar and 78foo brenden and a=b"); //Middle subclause invalid
_testFailure("foo=bar and \n78foo brenden and a=b"); //Middle subclause invalid with newline
_testFailure("foo=bar and not"); //Second subclause missing with not
_testFailure("foo=bar and not foo ="); //Second subclause incomplete with not
_testFailure("not"); //Just not
_testFailure("not not"); //two nots
_testFailure("a=b and not not=b"); //not instead of field
_testFailure("a=b not a=b"); //missing logical operator (not instead)
_testFailure("("); //just one paren
_testFailure("abc = ()"); //empty list
_testFailure("abc ~ ()"); //empty list with like

_testFailure("abc in ((fee, fie, foe, fum), 787, (34, (45))"); //complex nested parens don't match
_testFailure("priority = 12345=== not p jfkff fjfjfj"); //Random crap on the end
_testFailure("priority = 12345 \njfkff fjfjfj"); //Random crap on the end2
_testFailure("priority=a jfkff=fjfjfj"); //Random crap on the end3
_testFailure("priority=12345 jfkff=fjfjfj"); //Random crap on the end4
_testFailure("a=b ,b"); //Random crap on the end5
_testFailure("a=b,b"); //Random crap on the end6

//Come parens tests.
_testFailure("abc = )foo("); //backwards parens
_testFailure("(a in a"); //unmatched parens (open only)
_testFailure("abc in (foo"); //unmatched parens (open only)
_testFailure("abc in (foo("); //unmatched parens (open open)
_testFailure("abc IN ((foo) abc = a"); //unmatched parens (double open)
_testFailure("abc in (foo))"); //unmatched parens (open close close)
_testFailure("abc in ((foo), a"); //unmatched parens (open open)
_testFailure("(abc  =        b or not (j=k and p=l)"); //unmatched parens (open open close)

//Some CF tests.
_testFailure("cf[1234 = x"); //Unclosed CF label
_testFailure("cf1234] = x"); //Unopened CF label
_testFailure("cf[z123] = x"); //Non-numeric id in CF label
_testFailure("cf[-123] = x"); //Negative numeric id in CF label
// This test is irrelevant as integers are processed as strings in the front-end grammer
// _testFailure("cf[99999999999999999999999999999999999999999999999999999999999999999999999999] = x"); //Too big numeric id in CF label
_testFailure("cf[] = x"); //Empty id in CF label
_testFailure("[54] = x"); //Missing CF in CF label
_testFailure("cf = brenden"); //Custom Field (cf) without brackets
_testFailure("cf=brenden"); //Custom Field (cf) without brackets without spaces
_testFailure("cfasdasd=cf"); // cf cannot be used as an unquoted operand
_testFailure("cfasdasd = cf"); // cf cannot be used as an unquoted operand
_testSuccess("cfasdasd = brenden"); //Custom Field (cf) without brackets without spaces

//Lets test some bad function arguments.
_testFailure("q = func( ["); //Bad function argument
_testFailure("q = func(a, [)"); //Bad function argument2
_testFailure("q = func(a, ["); //Bad function argument3
_testFailure("q = func(a, )"); //Bad function argument4
_testFailure("q = func(a, "); //Bad function argument5
_testFailure("q = func(a"); //Bad function argument6
_testFailure("q = func("); //Bad function argument7
_testFailure("q = func(good, jack!, \"really good\")"); //Bad function argument8

_testFailure("pri notin (x)"); //No such operator
_testFailure("pri isnot empty"); //No such operator2
_testFailure("pri ^ empty"); //No such operator3
_testFailure("pri is"); //No such operator4
_testFailure("pri not is empty"); //No such operator5
_testFailure("pri ! in (test)"); //Bang does not work for 'not in'
_testFailure("pri is ! empty"); //Bang does not work for 'not empty'

_testFailure("priority = \"\"\""); //Unmatched string quote
_testFailure("priority = \""); //Unmatched string quote2
_testFailure("priority = '''"); //Unmatched string quote3
_testFailure("priority = '"); //Unmatched string quote4

_testFailure("what = hrejw'ewjrhejkw"); //Unescaped single quote
_testFailure("wh\"at = hrejwewjrhejkw"); //Unescaped quote

_testFailure("'' = bad"); //Empty Field Name
_testFailure("\"\" = bad"); //Empty Field Name2
_testFailure("\"\" = \\ "); //Empty Field Name2
_testFailure("\\     < 38"); //Empty Escaped Field Name
_testFailure("a \n= ''()"); //Empty Function Name
_testFailure("b = \"\"()"); //Empty Function Name2

_testFailure("test = case\\"); //Unterminated escape
_testFailure("test = case\\k"); //Illegal escape
_testFailure("test = case\\u"); //Unterminated illegal unicode
_testFailure("test = case\\u278q"); //Unterminated illegal unicode2
_testFailure("test = case\\u27"); //Unterminated illegal unicode3
_testFailure("test = case\\u-998"); //Unterminated illegal unicode4
_testFailure("test = case order by \\u-998"); //Unterminated illegal unicode5
_testFailure("test = case\\uzzzz"); //Unterminated illegal unicode6
_testFailure("test = case\\u278qzzzz"); //Unterminated illegal unicode7
_testFailure("test = case\\u27zzzzz"); //Unterminated illegal unicode8
_testFailure("tecase\\u-998zzzzz"); //Unterminated illegal unicode9
_testFailure("case order by \\u-998zzzzz"); //Unterminated illegal unicod10

// This test is irrelevant in the browser as numbers are not parsed, but remain as strings
// _testFailure("priority = 999999999999999999999999999999999999999999999999999999"); //Number too big
// _testFailure("priority in  (999999999999999999999999999999999999999999999999999999, 2)"); //Number too big 2

_testFailure("c = q\uffff"); //Control Character String
_testFailure("c = c\ufdd0"); //Control Character String 2
_testFailure("c = aa\ufdd0kkk"); //Control Character String 3
_testFailure("\u0000iuyiuyiu"); //Control Character String 4
_testFailure("aa = order by \u0001"); //Control Character String 5

_testFailure("control = \"char\ufffe\""); //Control Character Quoted
_testFailure("control = \"char\ufdef\""); //Control Character Quoted 2
_testFailure("\"\u001f\""); //Control Character Quoted 3
_testFailure("control = \"char\b\""); //Control Character Quoted 4
_testFailure("control = a order by \"char\b\""); //Control Character Quoted 5

_testFailure("control\n = 'char\u0002'"); //Control Character Single Quoted
_testFailure("control = '\ufdd5'"); //Control Character Single Quoted 2
_testFailure("control = '\ufdef'"); //Control Character Single Quoted 3
_testFailure("control = 'char\t'"); //Control Character Single Quoted 4
_testFailure("control = a order by 'char\t'"); //Control Character Single Quoted 5

_testFailure("cont\nrol = ''"); //Control Character Field Name
_testFailure("control = f\run()"); //Control Character Function Name
_testFailure("control = fun('\uffff')"); //Control Character Function Argument

_testFailure("a = b order by"); //Empty Order By
_testFailure("a = b order"); //Empty Order By2
_testFailure("a = b order bad"); //Empty Order By3
_testFailure("a = b order BY abc desc, "); //Unfinished Order By
_testFailure("a = b order BY abc desc, ]"); //Unfinished Order By 2
_testFailure("order BY abc s"); //Bad Order
_testFailure("order BY desc"); //Bad Name Order
_testFailure("order BY era desc extra"); //Order by Extra

_testFailure("query = hello\nworld"); //Test newline in value

// Change History tests
_testSuccess("assignee changed before now()");
_testSuccess("assignee was not delkan before now() AFTER endOfDay() BEFORE endOfMonth()");
_testSuccess("something changed from somethingElse");
_testSuccess("something changed to somethingElse");
_testSuccess("something changed to somethingElse by someone");
_testSuccess("something changed to somethingElse by (someone, someoneelse)");
_testSuccess("something changed to somethingElse during (sometime, someothertime)");
_testSuccess("something changed to somethingElse before 2012-01-01");
_testSuccess("something changed to somethingElse after 2012-01-01");
_testSuccess("something changed to somethingElse on 2012-01-01");
_testFailure("something changed somethingElse (fred, bob ");


// Dave added
_testSuccess("(priority=EMPTY)");
_testSuccess("(priority = EMPTY)");
_testSuccess("(priority=qwerty and something=somethingsuper)");
_testSuccess("(priority=qwerty and something=somethingsuper and something = somethingelse)");
_testSuccess("(priority=qwerty and something=somethingsuper or something = somethingelse)");
_testSuccess("(priority=qwerty and something=somethingsuper or (something = somethingelse))");
_testSuccess("(priority=qwerty and something=somethingsuper) or (something = somethingelse)");
_testSuccess("(priority=qwerty and something=somethingsuper or (something = somethingelse or another=woot)) order by something");
_testSuccess("priority=qwerty and something=something and somethingelse=somethingelse");

_testSuccess("order by priority");
_testSuccess("order by priority desc, other");
_testSuccess("  order by priority");
_testSuccess("  order by priority  ");
_testSuccess("order    by priority");
_testSuccess("priority = qwerty order by priority, other");
