var tap = require("tap"),
	test = tap.test,
	javascriptParser = require("../lib/grammer/javascript"),
	SUCCESS = true,
	successful = [];

function _testParse(query, expectedResult) {
	console.log("testing", query);
  try {
      return javascriptParser.parse(query);
  } catch (e) {
    console.log(e);
  }
}

_testParse("function() {");
_testParse("var something = '123'");
console.log(_testParse("'\\\\foo'"));
